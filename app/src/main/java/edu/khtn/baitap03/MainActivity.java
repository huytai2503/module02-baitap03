package edu.khtn.baitap03;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.net.URI;

public class MainActivity extends Activity {
    Intent intentSignIn, intentSignUp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnToSignIn = (Button) findViewById(R.id.btnToSignIn);
        Button btnToSignUp = (Button) findViewById(R.id.btnToSignUp);
        Intent intent = getIntent();
        String message = intent.getStringExtra(SignInActivity.EXTRA_MESSAGE);
        TextView txtName = (TextView) findViewById(R.id.txtNameSignIn);
        txtName.setText(message);
        intentSignIn = new Intent(this, SignInActivity.class);
        intentSignUp = new Intent(this, SignUpActivity.class);

        btnToSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intentSignIn);
                finish();
            }
        });
        btnToSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intentSignUp);
            }
        });
    }
}