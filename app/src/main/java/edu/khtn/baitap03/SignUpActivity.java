package edu.khtn.baitap03;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SignUpActivity extends Activity {
    EditText edtName, edtPass1, edtPass2;
    Button btnSignUp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        edtName = (EditText) findViewById(R.id.edtName);
        edtPass1 = (EditText) findViewById(R.id.edtPass1);
        edtPass2 = (EditText) findViewById(R.id.edtPass2);
        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pass1 = edtPass1.getText().toString();
                String pass2 = edtPass2.getText().toString();
                String name = edtName.getText().toString();
                if (name.isEmpty()) {
                    thongBao("Chưa nhập tên",false);
                }else if (pass1.isEmpty()||pass2.isEmpty()){
                    thongBao("Chưa nhập password",false);
                }else if (pass1.contentEquals(pass2)==false){
                    thongBao("Nhập lại password",false);
                }else{
                    ghiFile(name + "-" + pass1);
                    thongBao("Đăng ký thành công: "+"{"+name+","+pass1+"}",true);
                }
            }
        });
    }

    public void ghiFile(String s){
        try {
            FileOutputStream output = openFileOutput("UserFile", MODE_PRIVATE);
            char[] chars = s.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                output.write(chars[i]);
            }
            output.close();
        } catch (FileNotFoundException e) {
            thongBao("Không tìm thấy file", false);
        } catch (IOException e) {
            thongBao("Không thể ghi file", false);
        }
    }

    public void thongBao(String noiDung, Boolean exitOrNot){
        AlertDialog.Builder msg = new AlertDialog.Builder(this);
        msg.setTitle("Thông báo!");
        if (exitOrNot.booleanValue()) {
            msg.setMessage(noiDung);
            msg.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }else {
            msg.setMessage(noiDung);
            msg.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        msg.show();
    }
}
